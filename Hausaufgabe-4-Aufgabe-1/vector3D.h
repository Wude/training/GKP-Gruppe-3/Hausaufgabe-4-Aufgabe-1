#ifndef __VECTOR3D_H__
#define __VECTOR3D_H__

/*******************************************************************************
* Aufgabe 1.1
*******************************************************************************/

// Ein 3D-Vektor (als Array).
typedef double vector3Da_t[3];

// Einen 3D-Vektor (Array) von der Konsole lesen.
void vector3DaGetFromConsole(vector3Da_t result, char *message);

// Einen 3D-Vektor (Array) auf der Konsole ausgeben.
void vector3DaPut(vector3Da_t vector, char *message, ...);

// Mehrere 3D-Vektoren (Array) addieren und das Ergebnis liefern.
void vector3DaAddVA(vector3Da_t result, vector3Da_t operand, ...);

// Zwei 3D-Vektoren (Array) addieren und das Ergebnis liefern.
void vector3DaAdd(vector3Da_t result, vector3Da_t left, vector3Da_t right);

// Von einem 3D-Vektor (Array) einen anderen 3D-Vektor (Array) subtrahieren und das Ergebnis liefern.
void vector3DaSubtract(vector3Da_t result, vector3Da_t left, vector3Da_t right);

/*******************************************************************************
* Aufgabe 1.4 - Header
*******************************************************************************/

// Ein 3D-Vektor (als Struct).
struct vector3Ds
{
  double x;
  double y;
  double z;
};

// Ein 3D-Vektor (als Struct).
typedef struct vector3Ds vector3Ds_t;

// Mehrere 3D-Vektoren (Struct) addieren und das Ergebnis liefern.
vector3Ds_t vector3DsAddVA(vector3Ds_t *operand, ...);

// Einen 3D-Vektor (Struct) auf der Konsole ausgeben.
void vector3DsPut(vector3Ds_t *vector, char *message, ...);

// Einen 3D-Vektor (Struct) von der Konsole lesen.
vector3Ds_t vector3DsGetFromConsole(char *message);

// Zwei 3D-Vektoren (Struct) addieren und das Ergebnis liefern.
vector3Ds_t vector3DsAdd(vector3Ds_t *left, vector3Ds_t *right);

// Von einem 3D-Vektor (Struct) einen anderen 3D-Vektor (Struct) subtrahieren und das Ergebnis liefern.
vector3Ds_t vector3DsSubtract(vector3Ds_t *left, vector3Ds_t *right);

#endif
